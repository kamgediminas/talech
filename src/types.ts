export interface FormInterface {
  id?: number | null
  name: string
  serial: string
  type: string
  weight: string
  color: string
  quantity: string
  price: string
  active: boolean
  priceHistory?: { amount: string; time: number }[]
  quantityHistory?: { amount: string; time: number }[]
}
