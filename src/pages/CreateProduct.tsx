import React, { useState } from 'react'
import styled from 'styled-components'
import { useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import Alert from '../components/Alert'

import { FormInterface } from '../types'
import { saveProduct } from '../store/actions/productsActions'

import Form from '../components/Form'

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`

const CreateProduct = () => {
  const [success, setSuccess] = useState(false)

  const { t } = useTranslation()

  const dispatch = useDispatch()

  const submit = (body: FormInterface) => {
    dispatch(saveProduct(body))
    setSuccess(true)
  }

  const clearMsg = () => {
    setSuccess(false)
  }

  return (
    <Wrapper>
      {success ? <Alert text={t(`success.create`)} onPress={clearMsg} /> : null}
      <Form submit={submit} edit={false} />
    </Wrapper>
  )
}

export default CreateProduct
