import React from 'react'
import { useSelector } from 'react-redux'
import { RootState } from '../store/store'
import { useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import TabsMaterial from '@material-ui/core/Tabs'
import TabMaterial from '@material-ui/core/Tab'
import NoProduct from '../components/NoProduct'
import ProductCard from '../components/ProductCard'
import Chart from '../components/Chart'

import colors from '../styles/colors'
import fonts from '../styles/fonts'

type Params = {
  id: string
}

interface TabPanelProps {
  children?: React.ReactNode
  index: number
  value: number
}

const TabPanelWrapper = styled.div`
  margin: 3rem 0;
`
const Tabs = styled(TabsMaterial)`
  border-radius: 4px;
  background-color: ${colors.teaGreen};
  && {
    .MuiTabs-indicator {
      background-color: ${colors.grape};
    }
  }
`

const Tab = styled(TabMaterial)`
  .MuiTab-wrapper {
    color: ${colors.white};
    font-family: ${fonts.family};
    font-size: ${fonts.s16};
    text-transform: none;
  }
`

const TabPanel = (props: TabPanelProps) => {
  const { children, value, index, ...other } = props

  return (
    <TabPanelWrapper
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <div>{children}</div>}
    </TabPanelWrapper>
  )
}

const a11yProps = (index: number) => {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  }
}

const Product = () => {
  const [value, setValue] = React.useState(0)

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue)
  }

  let { id } = useParams<Params>()

  const { t } = useTranslation()

  const products = useSelector((state: RootState) => state.products.list)
  const foundProduct = products.find((product) => {
    // eslint-disable-next-line eqeqeq
    return product.id == id
  })

  if (!foundProduct) {
    return <NoProduct />
  }

  const { priceHistory, quantityHistory } = foundProduct

  return (
    <>
      <Tabs
        value={value}
        onChange={handleChange}
        aria-label="simple tabs example"
      >
        <Tab label={t(`product.info`)} {...a11yProps(0)} />
        <Tab label={t(`product.priceChartLabel`)} {...a11yProps(1)} />
        <Tab label={t(`product.quantityChartLabel`)} {...a11yProps(3)} />
      </Tabs>

      <TabPanel value={value} index={0}>
        <ProductCard product={foundProduct} />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Chart
          title={t(`product.priceChartLabel`)}
          data={priceHistory}
          name={t(`form.price`)}
        />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Chart
          title={t(`product.quantityChartLabel`)}
          data={quantityHistory}
          name={t(`form.quantity`)}
        />
      </TabPanel>
    </>
  )
}

export default Product
