import React, { useState } from 'react'
import styled from 'styled-components'
import { useTranslation } from 'react-i18next'
import { useDispatch } from 'react-redux'
import Alert from '../components/Alert'

import { FormInterface } from '../types'
import { editProduct } from '../store/actions/productsActions'

import Form from '../components/Form'

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`

const EditProduct = () => {
  const [success, setSuccess] = useState(false)

  const { t } = useTranslation()

  const dispatch = useDispatch()

  const submit = (body: FormInterface) => {
    dispatch(editProduct(body))
    setSuccess(true)
  }

  const clearMsg = () => {
    setSuccess(false)
  }

  return (
    <Wrapper>
      {success ? <Alert text={t(`success.edit`)} onPress={clearMsg} /> : null}
      <Form submit={submit} edit={true} />
    </Wrapper>
  )
}

export default EditProduct
