import React from 'react'
import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { RootState } from '../store/store'
import styled from 'styled-components'

import ProductCard from '../components/ProductCard'

import fonts from '../styles/fonts'

const Title = styled.h2`
  width: 100%;
  margin: 2rem 0;
  text-align: center;
  font-family: ${fonts.family};
  font-size: ${fonts.s24};
`

const None = styled.p`
  width: 100%;
  margin: 2rem 0;
  text-align: center;
  font-family: ${fonts.family};
  font-size: ${fonts.s16};
`

const Home = () => {
  const { t } = useTranslation()

  const products = useSelector((state: RootState) => state.products.list)

  const availableProducts = products.filter(
    (product) => product.active === true
  )

  return (
    <>
      <Title>{t(`home.availableProducts`)}</Title>

      {availableProducts.length ? (
        availableProducts.map((product, key: number) => {
          return <ProductCard product={product} key={key} />
        })
      ) : (
        <None>{t(`table.noProducts`)}</None>
      )}
    </>
  )
}

export default Home
