import { Dispatch } from 'redux'
import { FormInterface } from '../../types'

export const SAVE_PRODUCT = 'SAVE_PRODUCT'
export const EDIT_ACTIVE = 'EDIT_ACTIVE'
export const EDIT_PRODUCT = 'EDIT_PRODUCT'
export const DELETE_PRODUCT = 'DELETE_PRODUCT'

export const saveProduct = (product: FormInterface) => (dispatch: Dispatch) => {
  dispatch({
    type: SAVE_PRODUCT,
    payload: product,
  })
}

export const editProduct = (product: FormInterface) => (dispatch: Dispatch) => {
  dispatch({
    type: EDIT_PRODUCT,
    payload: product,
  })
}
export const editActive = (id: number) => (dispatch: Dispatch) => {
  dispatch({
    type: EDIT_ACTIVE,
    payload: { id },
  })
}

export const deleteProduct = (id: number) => (dispatch: Dispatch) => {
  dispatch({
    type: DELETE_PRODUCT,
    payload: { id },
  })
}
