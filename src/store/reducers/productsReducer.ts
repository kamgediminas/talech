import {
  DELETE_PRODUCT,
  SAVE_PRODUCT,
  EDIT_ACTIVE,
  EDIT_PRODUCT,
} from '../actions/productsActions'
import { FormInterface } from '../../types'

interface StateInterface {
  list: Array<any>
  idCounter: number
}

const initialState: StateInterface = {
  list: [],
  idCounter: 0,
}

const productsReducer = (
  state = initialState,
  action: { payload: FormInterface; type: string }
) => {
  const { payload, type } = action

  const productsClone = [...state.list]
  const foundIndex = productsClone.findIndex(
    (product: FormInterface) => product.id === payload.id
  )

  const price = payload?.price
  const quantity = payload?.quantity
  const time = Date.now()
  const priceHistory = {
    amount: price,
    time,
  }
  const quantityHistory = {
    amount: quantity,
    time,
  }
  switch (type) {
    case SAVE_PRODUCT:
      payload.id = state.idCounter
      payload.priceHistory = [priceHistory]
      payload.quantityHistory = [quantityHistory]
      return {
        ...state,
        list: [...state.list, payload],
        idCounter: state.idCounter + 1,
      }
    case EDIT_PRODUCT:
      const newPriceArr = productsClone[foundIndex].priceHistory
      newPriceArr.push(priceHistory)
      const newQuantityArr = productsClone[foundIndex].quantityHistory
      newQuantityArr.push(quantityHistory)
      productsClone[foundIndex] = payload
      productsClone[foundIndex].priceHistory = newPriceArr
      productsClone[foundIndex].quantityHistory = newQuantityArr
      return {
        ...state,
        list: productsClone,
      }
    case EDIT_ACTIVE:
      productsClone[foundIndex].active = !productsClone[foundIndex].active
      return {
        ...state,
        list: productsClone,
      }

    case DELETE_PRODUCT:
      const productsAfterDel = productsClone.filter(
        (product: FormInterface) => {
          return payload.id !== product.id
        }
      )
      return {
        ...state,
        list: productsAfterDel,
      }
    default:
      return initialState
  }
}

export default productsReducer
