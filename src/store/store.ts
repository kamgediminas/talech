import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import { persistStore, persistReducer } from 'redux-persist'

import productsReducer from './reducers/productsReducer'

import storage from 'redux-persist/lib/storage'

const initialState = {}
const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['products'],
}

const rootReducer = combineReducers({
  products: productsReducer,
})
const persistedReducer = persistReducer(persistConfig, rootReducer)
const store: any = createStore(
  persistedReducer,
  initialState,
  composeWithDevTools(applyMiddleware(thunkMiddleware))
)
const persistor = persistStore(store)
export { store, persistor }
export type RootState = ReturnType<typeof rootReducer>
