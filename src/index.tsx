import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'

import { Provider as StateProvider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

import { store, persistor } from './store/store'

ReactDOM.render(
  <React.StrictMode>
    <StateProvider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <App />
      </PersistGate>
    </StateProvider>
  </React.StrictMode>,
  document.getElementById('root')
)
