const fonts = {
  family: `'Roboto', sans-serif`,
  regular: '400',
  semi: '600',
  bold: '700',
  s10: '1rem',
  s16: '1.6rem',
  s24: '2.4rem',
  s36: '3.6rem',
}
export default fonts
