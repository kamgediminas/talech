const colors = {
  white: '#FFFFFF',
  black: '#000000',
  teaGreen: '#CCE2A3',
  grape: '#654F6F',
  blue: '#5C5D8D',
  red: '#F08080',
}
export default colors
