import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router-dom'
import {
  DataGrid as DataGridMaterial,
  GridOverlay,
  GridColDef,
  GridRowData,
} from '@material-ui/data-grid'
import styled from 'styled-components'

import { editActive, deleteProduct } from '../store/actions/productsActions'

import colors from '../styles/colors'
import fonts from '../styles/fonts'

import Checkbox from '../components/Checkbox'

const DataGrid = styled(DataGridMaterial)`
  .MuiDataGrid-row {
    background-color: 'red';
  }

  .MuiTypography-root {
    font-size: ${fonts.s16};
  }
  .MuiDataGrid-columnsContainer {
    background-color: ${colors.teaGreen};
    color: ${colors.white};
    font-size: ${fonts.s16};
  }
  .MuiDataGrid-cell {
    font-size: ${fonts.s16};
  }
  && {
    .MuiDataGrid-columnHeaderTitleContainer {
      padding: 0;
    }
    .MuiDataGrid-iconSeparator {
      display: none;
    }
  }
  .class-is-empty {
    background-color: ${colors.red};
  }
`

const ActionComponent = styled.a`
  margin-right: 1rem;
  cursor: pointer;
  &:hover {
    color: ${colors.grape};
  }
`

const NoRowsDiv = styled.div`
  font-size: ${fonts.s16};
`

const Table = () => {
  const rows = useSelector((state: GridRowData) => state.products.list)

  const { t } = useTranslation()

  const dispatch = useDispatch()

  const history = useHistory()

  const handleView = (id: number) => {
    history.push(`/product/${id}`)
  }

  const handleEdit = (id: number) => {
    history.push(`/product/edit/${id}`)
  }

  const handleActive = (id: number) => {
    dispatch(editActive(id))
  }

  const handleDelete = (id: number) => {
    dispatch(deleteProduct(id))
  }

  const renderActions = (row: GridRowData) => (
    <div>
      <ActionComponent onClick={() => handleView(row.id)}>
        {t('table.view')}
      </ActionComponent>
      <ActionComponent onClick={() => handleEdit(row.id)}>
        {t('table.edit')}
      </ActionComponent>
      <ActionComponent onClick={() => handleDelete(row.id)}>
        {t('table.delete')}
      </ActionComponent>
    </div>
  )

  const renderCheckbox = (row: GridRowData) => (
    <Checkbox checked={row.active} onChange={() => handleActive(row.id)} />
  )

  const CustomNoRowsOverlay = () => {
    return (
      <GridOverlay>
        <NoRowsDiv>{t('table.noProducts')}</NoRowsDiv>
      </GridOverlay>
    )
  }

  const columns: GridColDef[] = [
    {
      field: 'name',
      headerName: t('form.name'),
      sortable: false,
      disableColumnMenu: true,
      width: 130,
    },
    {
      field: 'serial',
      headerName: t('form.serial'),
      sortable: false,
      disableColumnMenu: true,
      width: 130,
    },
    {
      field: 'type',
      headerName: t('form.type'),
      sortable: false,
      disableColumnMenu: true,
      width: 130,
    },
    {
      field: 'weight',
      headerName: t('form.weight'),
      sortable: false,
      disableColumnMenu: true,
      width: 130,
    },
    {
      field: 'color',
      headerName: t('form.color'),
      sortable: false,
      disableColumnMenu: true,
      width: 130,
    },
    {
      field: 'quantity',
      headerName: t('form.quantity'),
      sortable: false,
      disableColumnMenu: true,
      width: 130,
    },
    {
      field: 'price',
      headerName: t('form.price'),
      sortable: false,
      disableColumnMenu: true,
      width: 130,
    },
    {
      field: 'active',
      headerName: t('form.active'),
      sortable: false,
      disableColumnMenu: true,
      renderCell: ({ row }) => renderCheckbox(row),

      width: 130,
    },
    {
      field: 'rating',
      headerName: t('table.actions'),
      sortable: false,
      disableColumnMenu: true,
      renderCell: ({ row }) => renderActions(row),
      width: 230,
    },
  ]

  return (
    <div style={{ height: 400 }}>
      <DataGrid
        rows={rows}
        columns={columns}
        getRowClassName={(params) => {
          if (parseInt(params.row.quantity) === 0) {
            return `class-is-empty`
          }
          return ''
        }}
        pageSize={5}
        checkboxSelection={false}
        disableSelectionOnClick
        components={{
          NoRowsOverlay: CustomNoRowsOverlay,
        }}
      />
    </div>
  )
}

export default Table
