import React from 'react'
import { Link as RouterLink } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import colors from '../styles/colors'
import fonts from '../styles/fonts'

const NavContainer = styled.nav`
  width: 100%;
  background-color: ${colors.teaGreen};
  border-bottom-left-radius: 4px;
  border-bottom-right-radius: 4px;
`
const LinksContainer = styled.ul`
  display: flex;
  justify-content: space-around;
`

const LinkWrapper = styled.li`
  padding: 2rem 1rem;
`

const Link = styled(RouterLink)`
  color: ${colors.white};
  font-family: ${fonts.family};
  font-weight: ${fonts.bold};
  font-size: ${fonts.s16};
  &:hover {
    color: ${colors.grape};
  }
`
const LngContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: flex-end;
`
const LngLink = styled.a`
  color: ${colors.grape};
  font-family: ${fonts.family};
  font-weight: ${fonts.bold};
  font-size: ${fonts.s16};
  cursor: pointer;
  margin: 1rem;
`
const Nav = () => {
  const { t, i18n } = useTranslation()

  const changeLanguage = (lng: string) => {
    i18n.changeLanguage(lng)
    localStorage.setItem('lng', lng)
  }

  return (
    <>
      <NavContainer>
        <LinksContainer>
          <LinkWrapper>
            <Link to="/">{t('menu.home')}</Link>
          </LinkWrapper>
          <LinkWrapper>
            <Link to="/products">{t('menu.products')}</Link>
          </LinkWrapper>
          <LinkWrapper>
            <Link to="/products/create">{t('menu.createProduct')}</Link>
          </LinkWrapper>
        </LinksContainer>
      </NavContainer>
      <LngContainer>
        <LngLink onClick={() => changeLanguage('en')}>EN</LngLink>
        <LngLink onClick={() => changeLanguage('lt')}>LT</LngLink>
      </LngContainer>
    </>
  )
}

export default Nav
