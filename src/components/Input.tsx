import React, { ChangeEvent } from 'react'
import styled from 'styled-components'

import colors from '../styles/colors'
import fonts from '../styles/fonts'

import FormControlMaterial from '@material-ui/core/FormControl'
import MaterialInput from '@material-ui/core/TextField'

import { FormikErrors } from 'formik'

interface PropsInterface {
  id?: string
  label: string
  value: string
  onChange: {
    (e: ChangeEvent<any>): void
    <T_1 = string | ChangeEvent<any>>(field: T_1): T_1 extends ChangeEvent<any>
      ? void
      : (e: string | ChangeEvent<any>) => void
  }
  error?: boolean
  helperText?:
    | string
    | false
    | string[]
    | FormikErrors<any>
    | FormikErrors<any>[]
    | undefined
}

const InputField = styled(MaterialInput)`
  .MuiInputBase-input {
    font-size: ${fonts.s16};
    color: ${colors.black};
  }
  .MuiFormLabel-root {
    font-size: ${fonts.s16};
  }
  .MuiFormLabel-root.Mui-focused {
    color: ${colors.grape};
  }
  .MuiFormLabel-root.Mui-error {
    color: #f44336;
  }
  .MuiFormHelperText-root {
    font-size: ${fonts.s10};
  }
  && {
    margin-bottom: 1rem;
    .MuiInput-underline::after {
      border-bottom: 0.2rem solid ${colors.grape};
    }
    .MuiInput-underline.Mui-error::after {
      transform: scaleX(1);
      border-bottom-color: #f44336;
    }
  }
`
const FormControl = styled(FormControlMaterial)`
  && {
    width: 20rem;
  }
`

const Input = ({
  id,
  label,
  value,
  onChange,
  error,
  helperText,
}: PropsInterface) => {
  return (
    <FormControl key={`input-${label}`}>
      <InputField
        id={id}
        value={value}
        onChange={onChange}
        helperText={helperText}
        error={error}
        label={label}
      />
    </FormControl>
  )
}

export default Input
