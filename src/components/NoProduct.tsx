import React from 'react'
import { useTranslation } from 'react-i18next'
import styled from 'styled-components'

import fonts from '../styles/fonts'

const NoProductDiv = styled.div`
  font-size: ${fonts.s16};
  font-family: ${fonts.family};
  text-align: center;
  width: 100%;
`

const NoProduct = () => {
  const { t } = useTranslation()

  return <NoProductDiv>{t('product.noProduct')}</NoProductDiv>
}

export default NoProduct
