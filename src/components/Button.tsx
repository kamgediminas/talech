import React from 'react'
import styled from 'styled-components'

import colors from '../styles/colors'
import fonts from '../styles/fonts'

import ButtonMaterial from '@material-ui/core/Button'

interface PropsInterface {
  title: string
  disabled?: boolean
  onClick?: () => void
}

const ButtonStyled = styled(ButtonMaterial)`
  && {
    margin: 1rem;
    background-color: ${colors.grape};
    color: ${colors.white};
    font-size: ${fonts.s16};
    &:hover {
      background-color: ${colors.blue};
    }
  }
`
const Button = ({ title, disabled, onClick }: PropsInterface) => {
  return (
    <ButtonStyled
      onClick={onClick}
      variant="contained"
      disabled={disabled}
      type={'submit'}
    >
      {title}
    </ButtonStyled>
  )
}

export default Button
