import React, { ChangeEvent } from 'react'
import CheckboxMaterial, { CheckboxProps } from '@material-ui/core/Checkbox'
import { withStyles } from '@material-ui/core/styles'
import FormControlLabelMaterial from '@material-ui/core/FormControlLabel'
import styled from 'styled-components'

import colors from '../styles/colors'
import fonts from '../styles/fonts'

interface PropsInterface {
  id?: string
  label?: string
  checked: boolean
  onChange: {
    (e: ChangeEvent<any>): void
    <T_1 = string | ChangeEvent<any>>(field: T_1): T_1 extends ChangeEvent<any>
      ? void
      : (e: string | ChangeEvent<any>) => void
  }
}

const FormControlLabel = styled(FormControlLabelMaterial)`
  && {
    width: 20rem;
    display: flex;
    margin: 1rem 0;
    align-items: center;
    justify-content: flex-end;
  }
  .MuiFormControlLabel-label {
    color: ${colors.black};
    font-size: ${fonts.s16};
    font-family: ${fonts.family};
  }
`
const CheckboxStyled = withStyles({
  root: {
    color: colors.grape,
    '&$checked': {
      color: colors.grape,
    },
  },
  checked: {},
})((props: CheckboxProps) => <CheckboxMaterial color="default" {...props} />)

const Checkbox = ({ id, label, checked, onChange }: PropsInterface) => {
  return (
    <FormControlLabel
      label={label}
      labelPlacement="start"
      control={
        <CheckboxStyled
          id={id}
          checked={checked}
          onChange={onChange}
          inputProps={{ 'aria-label': 'primary checkbox' }}
          style={{
            transform: 'scale(1.5)',
          }}
        />
      }
    />
  )
}

export default Checkbox
