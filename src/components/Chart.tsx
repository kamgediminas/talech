import React from 'react'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

import colors from '../styles/colors'
import fonts from '../styles/fonts'

interface Props {
  name: string
  title: string
  data: { amount: string; time: number }[]
}

const Chart = ({ title, data, name }: Props) => {
  let dataClone = [...data]
  // uncomment this to only show last 5 options
  // if (dataClone.length > 5) {
  //   dataClone = dataClone.slice(dataClone.length - 5)
  // }
  const dataDisplay = dataClone.map((item) => {
    return {
      x: item.time,
      y: parseInt(item.amount),
      name: new Date(item.time).toLocaleTimeString('en-US'),
      color: `${colors.teaGreen}`,
    }
  })

  const options: Highcharts.Options = {
    title: {
      text: title,
      style: {
        color: colors.black,
        fontFamily: fonts.family,

        fontSize: fonts.s24,
      },
    },
    series: [
      {
        type: 'line',
        data: dataDisplay,
        name: name,
      },
    ],
    colors: [colors.grape],
    yAxis: {
      labels: {
        style: {
          color: colors.black,
          fontFamily: fonts.family,
          fontWeight: fonts.regular,
        },
      },
      title: {
        text: name,
        style: {
          color: colors.black,
          fontFamily: fonts.family,
          fontSize: fonts.s16,
        },
      },
    },
    xAxis: {
      labels: {
        style: {
          color: colors.black,
          fontFamily: fonts.family,
          fontWeight: fonts.regular,
        },
        formatter: (data) => {
          return new Date(data.value).toLocaleDateString('en-US')
        },
      },
      title: {
        text: '',
      },
    },
    legend: {
      itemStyle: {
        color: colors.grape,
        fontFamily: fonts.family,
        fontSize: fonts.s16,
        fontWeight: fonts.regular,
      },
    },
  }

  return <HighchartsReact highcharts={Highcharts} options={options} />
}

export default Chart
