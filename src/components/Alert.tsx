import React from 'react'
import styled from 'styled-components'
import AlertMaterial from '@material-ui/lab/Alert'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'

import fonts from '../styles/fonts'

interface Props {
  text: string
  onPress: () => void
}

const AlertComponent = styled(AlertMaterial)`
  display: flex;
  align-items: center;
  width: 30rem;
  margin-bottom: 1rem;
  .MuiAlert-message {
    font-size: ${fonts.s16};
    font-family: ${fonts.family};
  }
`
const Alert = ({ text, onPress }: Props) => {
  return (
    <AlertComponent
      action={
        <IconButton
          aria-label="close"
          color="inherit"
          size="medium"
          onClick={onPress}
        >
          <CloseIcon fontSize="inherit" />
        </IconButton>
      }
      severity="success"
    >
      {`${text}!`}
    </AlertComponent>
  )
}

export default Alert
