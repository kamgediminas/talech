import React from 'react'
import styled from 'styled-components'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'
import { RootState } from '../store/store'
import { useParams } from 'react-router-dom'
import { useFormik } from 'formik'
import * as yup from 'yup'

import { FormInterface } from '../types'

import Input from '../components/Input'
import Button from '../components/Button'
import Checkbox from '../components/Checkbox'
import NoProduct from '../components/NoProduct'

interface Props {
  submit: (body: FormInterface) => void
  edit?: boolean
}
type Params = {
  id: string
}

const FormTable = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const Form = ({ submit, edit }: Props) => {
  const { t } = useTranslation()

  let { id } = useParams<Params>()

  const products = useSelector((state: RootState) => state.products.list)
  const foundProduct = products.find((product) => {
    // eslint-disable-next-line eqeqeq
    return product.id == id
  })

  const validationSchema = yup.object({
    name: yup.string().required(t('form.validation.name')),
    serial: yup
      .string()
      .min(8, t('form.validation.serial.min'))
      .required(t('form.validation.serial')),
    type: yup.string().required(t('form.validation.type')),
    weight: yup
      .number()
      .required(t('form.validation.weight'))
      .typeError(t('form.validation.number')),
    color: yup
      .string()
      .matches(/^[a-zA-Z]+$/, t('form.validation.string'))
      .required(t('form.validation.color')),
    quantity: yup
      .number()
      .required(t('form.validation.quantity'))
      .typeError(t('form.validation.number')),
    price: yup
      .number()
      .required(t('form.validation.price'))
      .typeError(t('form.validation.number')),
  })

  const formik = useFormik({
    initialValues: {
      name: foundProduct ? foundProduct.name : '',
      serial: foundProduct ? foundProduct.serial : '',
      type: foundProduct ? foundProduct.type : '',
      weight: foundProduct ? foundProduct.weight : '',
      color: foundProduct ? foundProduct.color : '',
      quantity: foundProduct ? foundProduct.quantity : '',
      price: foundProduct ? foundProduct.price : '',
      active: foundProduct ? foundProduct.active : false,
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      handleSubmit(values)
      if (!edit) {
        formik.resetForm()
      }
    },
  })

  const handleSubmit = (values: FormInterface) => {
    values.id = foundProduct ? foundProduct.id : null
    submit(values)
  }

  if (!foundProduct && edit) {
    return <NoProduct />
  }

  return (
    <FormTable onSubmit={formik.handleSubmit}>
      <Input
        id="name"
        label={t('form.name')}
        value={formik.values.name}
        onChange={formik.handleChange}
        error={formik.touched.name && Boolean(formik.errors.name)}
        helperText={formik.touched.name && formik.errors.name}
      />
      <Input
        id="serial"
        label={t('form.serial')}
        value={formik.values.serial}
        onChange={formik.handleChange}
        error={formik.touched.serial && Boolean(formik.errors.serial)}
        helperText={formik.touched.serial && formik.errors.serial}
      />
      <Input
        id="type"
        label={t('form.type')}
        value={formik.values.type}
        onChange={formik.handleChange}
        error={formik.touched.type && Boolean(formik.errors.type)}
        helperText={formik.touched.type && formik.errors.type}
      />
      <Input
        id="weight"
        label={t('form.weight')}
        value={formik.values.weight}
        onChange={formik.handleChange}
        error={formik.touched.weight && Boolean(formik.errors.weight)}
        helperText={formik.touched.weight && formik.errors.weight}
      />
      <Input
        id="color"
        label={t('form.color')}
        value={formik.values.color}
        onChange={formik.handleChange}
        error={formik.touched.color && Boolean(formik.errors.color)}
        helperText={formik.touched.color && formik.errors.color}
      />
      <Input
        id="quantity"
        label={t('form.quantity')}
        value={formik.values.quantity}
        onChange={formik.handleChange}
        error={formik.touched.quantity && Boolean(formik.errors.quantity)}
        helperText={formik.touched.quantity && formik.errors.quantity}
      />
      <Input
        id="price"
        label={t('form.price')}
        value={formik.values.price}
        onChange={formik.handleChange}
        error={formik.touched.price && Boolean(formik.errors.price)}
        helperText={formik.touched.price && formik.errors.price}
      />
      <Checkbox
        id="active"
        label={t('form.active')}
        checked={formik.values.active}
        onChange={formik.handleChange}
      />
      <Button title={t('form.submit')} />
    </FormTable>
  )
}

export default Form
