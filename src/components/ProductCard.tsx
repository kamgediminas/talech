import React from 'react'
import CardMaterial from '@material-ui/core/Card'
import CardContentMaterial from '@material-ui/core/CardContent'
import { FormInterface } from '../types'
import { useTranslation } from 'react-i18next'

import styled from 'styled-components'
import fonts from '../styles/fonts'

const Container = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  margin: 2rem 0;
`

const Card = styled(CardMaterial)`
  width: 34rem;
`
const CardContent = styled(CardContentMaterial)`
  font-family: ${fonts.family};
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

const Label = styled.p`
  font-size: ${fonts.s16};
  font-weight: ${fonts.bold};
`

const Value = styled.p`
  font-size: ${fonts.s16};
`

interface Props {
  product: FormInterface
}
const ProductCard = ({ product }: Props) => {
  const { t } = useTranslation()

  return (
    <Container>
      <Card>
        <CardContent>
          <Label>{t('form.name')}</Label>
          <Value>{product.name}</Value>
        </CardContent>
        <CardContent>
          <Label>{t('form.serial')}</Label>
          <Value>{product.serial}</Value>
        </CardContent>
        <CardContent>
          <Label>{t('form.type')}</Label>
          <Value>{product.type}</Value>
        </CardContent>
        <CardContent>
          <Label>{t('form.weight')}</Label>
          <Value>{product.weight}</Value>
        </CardContent>
        <CardContent>
          <Label>{t('form.color')}</Label>
          <Value>{product.color}</Value>
        </CardContent>
        <CardContent>
          <Label>{t('form.quantity')}</Label>
          <Value>{product.quantity}</Value>
        </CardContent>
        <CardContent>
          <Label>{t('form.price')}</Label>
          <Value>{product.price}</Value>
        </CardContent>
        <CardContent>
          <Label>{t('form.active')}</Label>
          <Value>{product.active ? t(`form.yes`) : t(`form.no`)}</Value>
        </CardContent>
      </Card>
    </Container>
  )
}

export default ProductCard
