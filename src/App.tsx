import React, { Suspense } from 'react'
import './i18n'
import './App.css'
import Nav from './components/Nav'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Home from './pages/Home'
import Products from './pages/Products'
import Product from './pages/Product'
import EditProduct from './pages/EditProduct'
import CreateProduct from './pages/CreateProduct'

const App = () => {
  return (
    <Suspense fallback={null}>
      <Router>
        <Nav />
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/product/:id">
            <Product />
          </Route>
          <Route exact path="/products">
            <Products />
          </Route>
          <Route exact path="/product/edit/:id">
            <EditProduct />
          </Route>
          <Route exact path="/products/create">
            <CreateProduct />
          </Route>
        </Switch>
      </Router>
    </Suspense>
  )
}

export default App
